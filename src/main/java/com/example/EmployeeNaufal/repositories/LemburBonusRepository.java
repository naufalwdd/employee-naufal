package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.LemburBonus;

public interface LemburBonusRepository extends JpaRepository<LemburBonus, Long> {

}
