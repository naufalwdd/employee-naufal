package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.ParameterPajak;

public interface ParameterPajakRepository extends JpaRepository<ParameterPajak, Long> {

}
