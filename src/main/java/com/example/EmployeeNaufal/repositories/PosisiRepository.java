package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.Posisi;

public interface PosisiRepository extends JpaRepository<Posisi, Long> {

}
