package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.Karyawan;

public interface KaryawanRepository extends JpaRepository<Karyawan, Long>{

}
