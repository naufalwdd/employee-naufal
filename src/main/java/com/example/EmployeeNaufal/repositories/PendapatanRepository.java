package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.Pendapatan;

public interface PendapatanRepository extends JpaRepository<Pendapatan, Long>{

}
