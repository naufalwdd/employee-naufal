package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.User;
import com.example.EmployeeNaufal.models.UserId;

public interface UserRepository extends JpaRepository<User, UserId> {

}
