package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.TunjanganPegawai;

public interface TunjanganPegawaiRepository extends JpaRepository<TunjanganPegawai, Long> {

}
