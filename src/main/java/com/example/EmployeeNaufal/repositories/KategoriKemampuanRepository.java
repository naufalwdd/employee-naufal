package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.KategoriKemampuan;

public interface KategoriKemampuanRepository extends JpaRepository<KategoriKemampuan, Long> {

}
