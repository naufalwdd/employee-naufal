package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.Penempatan;

public interface PenempatanRepository extends JpaRepository<Penempatan, Long> {

}
