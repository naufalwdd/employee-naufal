package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.Parameter;

public interface ParameterRepository extends JpaRepository<Parameter, Long> {

}
