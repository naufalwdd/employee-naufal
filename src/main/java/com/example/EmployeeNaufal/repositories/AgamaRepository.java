package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.Agama;

public interface AgamaRepository extends JpaRepository<Agama, Long> {

}
