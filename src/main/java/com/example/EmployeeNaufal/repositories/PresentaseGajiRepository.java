package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.PresentaseGaji;

public interface PresentaseGajiRepository extends JpaRepository<PresentaseGaji, Long> {

}
