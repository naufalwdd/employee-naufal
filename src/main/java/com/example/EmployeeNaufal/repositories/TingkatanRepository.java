package com.example.EmployeeNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeNaufal.models.Tingkatan;

public interface TingkatanRepository extends JpaRepository<Tingkatan, Long> {

}
