package com.example.EmployeeNaufal.dtos;

public class UserIdDto {

	private long idUser;
	private String username;
	
	// Constructor
	public UserIdDto() {
		// TODO Auto-generated constructor stub
	}

	public UserIdDto(long idUser, String username) {
		super();
		this.idUser = idUser;
		this.username = username;
	}

	// Getter Setter
	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
