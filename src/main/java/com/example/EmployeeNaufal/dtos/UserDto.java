package com.example.EmployeeNaufal.dtos;

public class UserDto {

	private UserIdDto id;
	private String password;
	private Short status;
	
	// Constructor
	public UserDto() {
		// TODO Auto-generated constructor stub
	}

	public UserDto(UserIdDto id, String password, Short status) {
		super();
		this.id = id;
		this.password = password;
		this.status = status;
	}

	// Getter Setter
	public UserIdDto getId() {
		return id;
	}

	public void setId(UserIdDto id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}
	
}
