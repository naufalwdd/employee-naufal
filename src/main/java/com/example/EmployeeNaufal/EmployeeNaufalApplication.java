package com.example.EmployeeNaufal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeNaufalApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeNaufalApplication.class, args);
	}

}
